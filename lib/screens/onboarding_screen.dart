import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_interview_task/config/palette.dart';
import 'package:flutter_interview_task/routing/routing_constants.dart';
import 'package:flutter_interview_task/shared_widgets/shared_widgets.dart';

class OnBoardingScreen extends StatelessWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            'assets/images/onBoarding.png',
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
          Padding(
            padding: const EdgeInsets.only(
                top: 40.0, left: 17.0, right: 17.0, bottom: 57.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  'assets/images/logo.png',
                  width: MediaQuery.of(context).size.width * 0.26,
                  height: MediaQuery.of(context).size.height * 0.05,
                ),
                Column(
                  children: [
                    _OnBoardingPageCounter(),
                    SizedBox(height: 22.0),
                    Text(
                      'Too tired to walk your dog? Let’s help you!',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 22.0,
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 22.0),
                    CustomTextButton(
                      onPressed: () {
                        Navigator.of(context).pushNamed(SignUpScreenRoute);
                      },
                    ),
                    SizedBox(height: 22.0),
                    RichText(
                      text: TextSpan(children: [
                        TextSpan(
                          text: 'Already a member? ',
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 13.0,
                          ),
                        ),
                        TextSpan(
                          text: "Sign in",
                          style: TextStyle(
                            color: Palette.orangeColor,
                            fontSize: 13.0,
                            fontWeight: FontWeight.w700,
                          ),
                        )
                      ]),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _OnBoardingPageCounter extends StatelessWidget {
  const _OnBoardingPageCounter({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _roundContainer(pageNum: 1),
        _customDivider(),
        _roundContainer(pageNum: 2),
        _customDivider(),
        _roundContainer(pageNum: 3),
      ],
    );
  }

  Container _roundContainer(
      {required int pageNum, Color color = Palette.whiteColor}) {
    return Container(
      decoration: BoxDecoration(
        color: color,
        shape: BoxShape.circle,
      ),
      width: 30.0,
      height: 30.0,
      child: Center(
        child: Text(
          pageNum.toString(),
          style: TextStyle(
            color: Palette.textBlack,
            fontSize: 13.0,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  Padding _customDivider() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      child: Container(
        height: 1.0,
        width: 10.0,
        color: Colors.white,
      ),
    );
  }
}
