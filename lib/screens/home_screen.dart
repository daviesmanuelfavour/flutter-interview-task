import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_interview_task/config/constants.dart';
import 'package:flutter_interview_task/config/palette.dart';
import 'package:flutter_interview_task/shared_widgets/shared_widgets.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFCFCFC),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(
            top: 40.0, left: 17.0, bottom: 23.0, right: 17.0),
        child: Column(
          children: [
            _TopColumn(),
            SizedBox(height: 10.0),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  _ImageColumn(),
                  _ImageColumn(),
                  _ImageColumn(),
                  _ImageColumn(),
                ],
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Divider(
              thickness: 2.0,
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Suggested',
                  style: Constants.headingStyle,
                ),
                Text(
                  'View all',
                  style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.w400,
                    color: Palette.textBlack,
                    decoration: TextDecoration.underline,
                  ),
                )
              ],
            ),
            SizedBox(height: 20.0),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  _ImageColumn(),
                  _ImageColumn(),
                  _ImageColumn(),
                  _ImageColumn(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _ImageColumn extends StatelessWidget {
  const _ImageColumn({
    Key? key,
    this.personNearYou = "Mason York",
    this.dis = '7',
  }) : super(key: key);

  final String personNearYou;
  final String dis;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: 125.0,
            width: 179.0,
            decoration: BoxDecoration(
                color: Colors.grey, borderRadius: BorderRadius.circular(14)),
            child: Image.asset('assets/images/frame.png'),
          ),
          SizedBox(height: 10.0),
          Text('$personNearYou'),
          SizedBox(height: 5.0),
          Row(
            children: [
              Icon(
                Icons.location_on_outlined,
                size: 15.0,
              ),
              Text('$dis km from you'),
            ],
          )
        ],
      ),
    );
  }
}

class _TopColumn extends StatelessWidget {
  const _TopColumn({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: const [
                Text(
                  'Home',
                  style: Constants.headingStyle,
                ),
                SizedBox(height: 15.0),
                Text('Explore dog walkers', style: Constants.subHeadingStyle),
                SizedBox(height: 22.0),
              ],
            ),
            const _CustomButton(),
          ],
        ),
        CustomTextField(),
        SizedBox(height: 22.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Near You',
              style: Constants.headingStyle,
            ),
            Text(
              'View all',
              style: TextStyle(
                fontSize: 15.0,
                fontWeight: FontWeight.w400,
                color: Palette.textBlack,
                decoration: TextDecoration.underline,
              ),
            )
          ],
        )
      ],
    );
  }
}

class _CustomButton extends StatelessWidget {
  const _CustomButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 104.0,
      height: 41.0,
      color: Colors.red,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: const [
          Icon(
            Icons.add,
            size: 20.0,
          ),
          Text(
            'Book a walk',
            style: TextStyle(
              fontSize: 10.0,
              fontWeight: FontWeight.w700,
              color: Colors.white,
            ),
          )
        ],
      ),
    );
  }
}
