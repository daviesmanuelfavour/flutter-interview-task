import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_interview_task/config/constants.dart';
import 'package:flutter_interview_task/config/palette.dart';
import 'package:flutter_interview_task/routing/routing_constants.dart';
import 'package:flutter_interview_task/shared_widgets/shared_widgets.dart';

class SignUpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        leading: GestureDetector(
          // onTap: () => Navigator.of(context).pop(),
          child: Icon(
            Icons.arrow_back_rounded,
            color: Palette.textBlack,
            size: 35.0,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(
          left: 17.0,
          right: 17.0,
        ),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Lets's start here",
                style: Constants.headingStyle,
              ),
              SizedBox(height: 5.0),
              Text(
                'Fill in your details to begin',
                style: Constants.subHeadingStyle,
              ),
              SizedBox(height: 22.0),
              CustomFormField(
                hintText: 'Fullname',
              ),
              SizedBox(height: 22.0),
              CustomFormField(
                hintText: 'Email',
              ),
              SizedBox(height: 22.0),
              CustomFormField(
                hintText: 'Password',
              ),
              SizedBox(height: 22.0),
              Align(
                  alignment: Alignment.center,
                  child: CustomTextButton(
                    label: 'Sign up',
                    onPressed: () {
                      Navigator.of(context).pushNamed(HomeScreenRoute);
                    },
                  )),
              SizedBox(height: 150.0),
              Align(
                child: SizedBox(
                  width: 320,
                  child: RichText(
                    textAlign: TextAlign.center,
                    text: TextSpan(children: [
                      TextSpan(
                        text: 'By signing in, I agree with Terms of Use and ',
                        style: TextStyle(
                          color: Palette.faintTextBlack,
                          fontSize: 13.0,
                        ),
                      ),
                      TextSpan(
                        text: "Privacy Policy",
                        style: TextStyle(
                          color: Palette.textBlack,
                          fontSize: 13.0,
                          fontWeight: FontWeight.w700,
                        ),
                      )
                    ]),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
