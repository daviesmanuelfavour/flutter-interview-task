import 'package:flutter/material.dart';
import 'package:flutter_interview_task/routing/routing_constants.dart';
import 'package:flutter_interview_task/screens/home_screen.dart';
import 'package:flutter_interview_task/screens/onboarding_screen.dart';
import 'package:flutter_interview_task/screens/signup_screen.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case OnBoardingScreenRoute:
      return MaterialPageRoute(builder: (context) => OnBoardingScreen());
    case SignUpScreenRoute:
      return MaterialPageRoute(builder: (context) => SignUpScreen());
    case HomeScreenRoute:
      return MaterialPageRoute(builder: (context) => HomeScreen());
    default:
      return MaterialPageRoute(builder: (context) => HomeScreen());
  }
}
