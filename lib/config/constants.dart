import 'package:flutter/material.dart';
import 'package:flutter_interview_task/config/palette.dart';

class Constants {
  static const TextStyle headingStyle = TextStyle(
    color: Palette.textBlack,
    fontSize: 34.0,
    fontWeight: FontWeight.w700,
  );
  static const TextStyle subHeadingStyle = TextStyle(
    color: Palette.faintTextBlack,
    fontSize: 25.0,
    fontWeight: FontWeight.w500,
  );
  static const TextStyle titleStyle = TextStyle(
    color: Palette.textBlack,
    fontSize: 27.0,
    fontWeight: FontWeight.w500,
  );
}
