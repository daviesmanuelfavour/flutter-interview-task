import 'package:flutter/material.dart';

class Palette {
  static const Color scaffold = Color(0xFF202020);

  static const Color logoColor = Color(0xFFE73A40);

  static const LinearGradient actionButtonGradient = LinearGradient(
    colors: [Color(0xFFFE904B), Color(0xFFFB724C)],
  );

  static const Color textBlack = Color(0xFF2B2B2B);
  static const Color faintTextBlack = Color(0xFFB0B0B0);

  static const Color orangeColor = Color(0xFFFB724C);
  static const Color whiteColor = Color(0xFFFCFCFC);
}
