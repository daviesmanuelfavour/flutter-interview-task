import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_interview_task/config/palette.dart';

class CustomTextButton extends StatelessWidget {
  final String label;
  final onPressed;

  CustomTextButton({
    this.label = "Join our community",
    this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 58,
      width: 320,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(14),
        gradient: LinearGradient(colors: [
          Color(0xFFFB724C),
          Color(0xFFFE904B),
        ]),
      ),
      child: TextButton(
        style: TextButton.styleFrom(
          padding: EdgeInsets.symmetric(vertical: 16),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          primary: Colors.transparent,
          shadowColor: Colors.transparent,
        ),
        onPressed: onPressed,
        child: Text(
          label,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 17.0,
            color: Palette.whiteColor,
          ),
        ),
      ),
    );
  }
}
