import 'package:flutter/material.dart';
import 'package:flutter_interview_task/config/palette.dart';

class CustomFormField extends StatelessWidget {
  const CustomFormField(
      {Key? key, this.hintText = 'Email', this.icon = Icons.password})
      : super(key: key);
  final String hintText;
  final IconData icon;

  final OutlineInputBorder _borderStyle = const OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.transparent,
    ),
    borderRadius: BorderRadius.all(Radius.circular(14.0)),
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10.0),
      height: 60.0,
      decoration: BoxDecoration(
        color: Color(0xFFF0F0F0),
        borderRadius: BorderRadius.all(
          Radius.circular(17.0),
        ),
      ),
      child: TextFormField(
        cursorColor: Palette.textBlack,
        style: TextStyle(
          color: Palette.textBlack,
          fontSize: 17.0,
        ),
        decoration: InputDecoration(
          labelText: hintText,
          labelStyle: TextStyle(fontSize: 17, color: Color(0xFFAEAEB2)),
          contentPadding: EdgeInsets.only(
            left: 18.0,
          ),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          fillColor: const Color(0xff146C2C00),
          focusColor: const Color(0xff6C2C00),
          // labelText: hintText,
          border: InputBorder.none,

          // enabledBorder: _borderStyle,
          // focusedBorder: _borderStyle,
        ),
      ),
    );
  }
}
