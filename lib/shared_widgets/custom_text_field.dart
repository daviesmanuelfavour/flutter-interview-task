import 'package:flutter/material.dart';
import 'package:flutter_interview_task/config/palette.dart';

class CustomTextField extends StatelessWidget {
  CustomTextField({Key? key, this.hintText = "Kiyv, Ukraine"})
      : super(key: key);
  final String hintText;

  final OutlineInputBorder _borderStyle = const OutlineInputBorder(
    borderSide: BorderSide(
      color: Colors.transparent,
    ),
    borderRadius: BorderRadius.all(Radius.circular(14.0)),
  );

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        filled: true,
        fillColor: const Color(0xff146C2C00),
        focusColor: const Color(0xff6C2C00),
        prefixIcon: const Icon(
          Icons.location_on_outlined,
          color: Palette.faintTextBlack,
        ),
        suffixIcon: const Icon(
          Icons.settings,
          color: Palette.faintTextBlack,
        ),
        floatingLabelBehavior: FloatingLabelBehavior.never,
        labelText: hintText,
        enabledBorder: _borderStyle,
        focusedBorder: InputBorder.none,
      ),
    );
  }
}
