import 'package:flutter/material.dart';
import 'package:flutter_interview_task/config/palette.dart';
import 'package:flutter_interview_task/routing/routing_constants.dart';
import 'package:flutter_interview_task/screens/home_screen.dart';
import 'package:flutter_interview_task/screens/onboarding_screen.dart';
import 'package:flutter_interview_task/screens/signup_screen.dart';
import 'routing/router.dart' as router;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Woo Dog',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: Palette.scaffold,
      ),
      onGenerateRoute: router.generateRoute,
      initialRoute: OnBoardingScreenRoute,
    );
  }
}
